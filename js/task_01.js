class Machine {
  constructor(name = 'Механизм'){
    this.name = name;
    this.activate = false;
  }

  turnOn(){
    this.activate = true;
    console.log(`${this.name} активирован(а)`);
  }
  turnOff(){
    this.activate = false;
    console.log(`${this.name} деактивирован(а)`);
  }
}

class HomeAppliance extends Machine {
  constructor(name = 'Бытовой прибор') {
    super();
    this.name = name;
    this.plug = false;
  }

  plugIn() {
    this.plug = true;
    console.log(`${this.name} включен(а) в сеть`);
  }
  plugOff() {
    this.plug = false;
    console.log(`${this.name} выключен(а) из сети`);
  }
}

class WashingMachine extends HomeAppliance {

  run(){
    if (this.activate && this.plug) {
      console.log(`${this.name} начал(а) работать`);
    } else {
      console.log(`${this.name} не включен`);
    }
  }
}

class LightSource extends HomeAppliance {
  constructor(name) {
    super(name)
    this.level = 0;
  }

  setLevel(level){
    if (!this.activate) {
      console.log(`${this.name} не включен(а)`);
    } else if (!this.plug) {
      console.log(`${this.name} не подлкючен в сеть`);
    } else if (level !== 0 && (level >= 1 && level <= 100)) {
      this.level = level;
      console.log(`Уровень освещенности - ${this.level} люкс(ов)`);
    } else {
      console.log('Укажите правильно параметр освещенности');
    }
  }
}

class AutoVehicle extends Machine {
  constructor(name) {
    super(name);
    this.x = 0;
    this.y = 0;
  }

  setPosition(x, y) {
    this.x = x;
    this.y = y;

    console.log(`Установлены начальные координаты движения: (${this.x}, ${this.y})`);
  }
}

class Car extends AutoVehicle {
  constructor(name) {
    super(name);
    this.speed = 10;
  }

  setSpeed(speed) {
    if (!this.activate) {
      console.log(`${this.name} не заведен(а)`);
    } else {
      this.speed = speed;
      console.log(`Скорость ${this.name} равна ${this.speed}`);
    }
  }

  run(x, y) {
    if (!this.activate) {
      console.log(`${this.name} не заведен(а)`);
    } else {
      console.log(`Задан пункт назначения: (${x}, ${y}), поехали`);
      let self = this;
  
      let drive = setInterval(function() {
        if (self.speed < 0) {
          clearInterval(drive);
          console.log('Нужно ехать только вперед');
        } else {
          if (self.x !== x && self.x < x) {
            self.x += self.speed;
            if (self.x > x) {
              self.x = x;
            }
          }
          if (self.y !== y && self.y < y) {
            self.y += self.speed;
            if (self.y > y) {
              self.y = y;
            }
          }
    
          console.log(self.x, self.y);
    
          if (self.x >= x && self.y >= x) {
            clearInterval(drive);
            console.log(`Приехали!!! Ваше место назначения: ${self.x}, ${self.y}`);
          }
        }
      }, 1000);
    }
  }
}


// let bosch = new WashingMachine('Стиралка Bosch');
// bosch.plugIn();
// bosch.turnOn();
// bosch.run();

// let lightBulb = new LightSource('Лампа');
// lightBulb.plugIn();
// lightBulb.setLevel(50);
// lightBulb.turnOn();
// lightBulb.setLevel(50);

var honda = new Car('Автомобиль Honda');
honda.setPosition(30, 40);
honda.turnOn();
honda.setSpeed(60);
// honda.turnOff();
honda.run(280, 340);